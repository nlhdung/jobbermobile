//
//  AppDelegate.swift
//  PoolPath
//
//  Created by Dung L.H. Nguyen on 7/25/16.
//  Copyright © 2016 DnDStudio. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        // TestQuang()
        // TestThanh()
        return true
    }
    
    func TestQuang(){
        Service.Login(Global.TestData.Email, password: Global.TestData.Password){
            data in
            
//            Service.ClientCreate("Mr", firstname: "Quang", lastname: "Teo", company: "ABC", email: "tdquang7@gmail.com", tel: "78612115"){
//                result in
//                
//                Service.ClientIndex(){
//                    data in
//                    print(data.count)
//                }
//            }
            
//            Service.ClientShow("213"){
//                client in
//            }
//            Service.PropertyIndex(){
//                data in
//                print(data.count)
//            }
            
//            Service.PropertyCreate("Test street", city: "City Name", state: "Test state", zipcode: "999008867", country: "Viet nam", clientid: 95 ){
//                result in
//                
//                print(result.Status)
//            }
            
//            Service.PropertyShow(249){
//                data in
//                
//                print(data)
//            }
//            Service.QuoteShow(165){
//                list in
//                print(list)
//            }
            
            Service.QuoteIndex(){
                data in
            }
            
//            Service.QuoteCreate("249", quote_order: 15, tax: 5, discount: 2, work_order_name: "test", work_order_description: "test quotes", client_message: "test message")
        }
    }
    
    func TestThanh(){
        if Global.DEBUG {
            print("Test Thanh")
            Service.TaskCreate("nlhdung task - all day", allDay: 1, startAtDate: "2016-08-11", startAtTime: "09:30 PM", endAtDate: "2016-08-12", endAtTime: "09:30 PM", description: "nlhdung", userId: 61)
            {
                info in
            }
            Service.TaskCreate("nlhdung task", allDay: 0, startAtDate: "2016-08-12", startAtTime: "00:10 AM", endAtDate: "2016-08-12", endAtTime: "09:30 PM", description: "nlhdung", userId: 61)
            {
                info in
            }
//            Service.GetTasks(61)
//            {
//                info in
//            }
            
            Service.TaskIndex(61)
            {
                info in
                print(info)
            }
        }
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

