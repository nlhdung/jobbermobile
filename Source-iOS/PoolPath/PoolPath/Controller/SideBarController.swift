//
//  SideBarController.swift
//  PoolPath
//
//  Created by quang on 7/26/16.
//  Copyright © 2016 DnDStudio. All rights reserved.
//

import UIKit

class SideBarController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    // MARK: Models
    let actions = ["Dashboard", "Timesheets", "Call office ()", "Clients", "Tasks", "Quotes", "Jobs", "Invoices", "Expense", "Chemicals", "Account & Settings", "Logout"]
    let images = ["dashboard.png", "timesheet.png", "call.png", "client.png", "task.png", "quote.png", "job.png", "invoice.png", "expense.png", "chemical.png", "account.png", "logout.png"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblActions.delegate = self
        tblActions.dataSource = self
    }
    
    // MARK: UI Elements
    @IBOutlet weak var tblActions: UITableView!
    
    
    // MARK: Table view
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return actions.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tblActions.dequeueReusableCellWithIdentifier("SideBarCellID", forIndexPath: indexPath) as! SideBarCell
        cell.lblAction.text = actions[indexPath.row]
        cell.imgIcon.image = UIImage(named: images[indexPath.row])
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let action = actions[indexPath.row]
        
        if action == "Logout" {
            // TODO: Gọi service logout?
            Global.User.Logout()
            UI.Segue(self.revealViewController(), id: "SegueLogin")
        } else if indexPath.row == 2 { // Call office
            
        } else if action == "Account & Settings" {
            UI.Root.showViewController(UI.Get("Main", idVC: "AccountID"), sender: true)
            revealViewController().revealToggle(nil)
        } else {
            UI.Root.showViewController(UI.Get("Main", idVC: actions[indexPath.row] + "ID"), sender: true)
            revealViewController().revealToggle(nil)
        }
    }
}
