//
//  TaskController.swift
//  PoolPath
//
//  Created by quang on 7/27/16.
//  Copyright © 2016 DnDStudio. All rights reserved.
//

import UIKit

class TaskController: UIViewController {
    // MARK: Local variables
    var task: BasicTask
    
    // UI elements
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblStreet: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblClient: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblAssignees: UILabel!
    
    // MARK: UI events
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let menuButton = UIBarButtonItem(image: UIImage(named: "reveal-icon"), style: UIBarButtonItemStyle.Plain, target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
        let dashboard = UIBarButtonItem(title: "Tasks", style: .Plain, target: nil, action: nil)
        self.navigationItem.setLeftBarButtonItems([menuButton, dashboard], animated: true)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer()) // vuốt để chuyển lại màn hình chính

    }
    
    // MARK: DAO
    
}
