//
//  TaskListCell.swift
//  PoolPath
//
//  Created by HTThanh on 7/27/16.
//  Copyright © 2016 DnDStudio. All rights reserved.
//

import UIKit

class TaskListCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblStartEndDate: UILabel!
    @IBOutlet weak var lblAssignees: UILabel!
}
