//
//  TaskListController.swift
//  PoolPath
//
//  Created by HTThanh on 7/27/16.
//  Copyright © 2016 DnDStudio. All rights reserved.
//

import UIKit

class TaskListController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    // MARK: Local variables
    var lst: [BasicTask] = []
    
    // MARK: Model
    var todayLst: [BasicTask] = []
    var dueBeforeTodayLst: [BasicTask] = []
    var requiringAssignmentLst: [BasicTask] = []
    
    // MARK: UI elements
    @IBOutlet weak var lblNumDueBeforeTodayTasks: UILabel!
    @IBOutlet weak var lblNumRequiringAssignmentTasks: UILabel!
    @IBOutlet weak var lblNumTodayTasks: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: UI events
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadTasks()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let menuButton = UIBarButtonItem(image: UIImage(named: "reveal-icon"), style: UIBarButtonItemStyle.Plain, target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
        let dashboard = UIBarButtonItem(title: "Dashboard", style: .Plain, target: nil, action: nil)
        self.navigationItem.setLeftBarButtonItems([menuButton, dashboard], animated: true)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer()) // vuốt để chuyển lại màn hình chính
        
        UI.Root = self.navigationController!
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "SegueTaskDetail" {
            var taskController = segue.destinationViewController as! TaskController
            
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let task = self.todayLst[indexPath.row]
                taskController.task = task
            }
        }
    }
    
    // MARK: Table view functions
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.todayLst.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("todayListCell", forIndexPath: indexPath) as! TaskListCell
        
        // Show task title
        cell.lblTitle.text = todayLst[indexPath.item].title
        
        // Show task start and end date
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        var s = ""
        if let startDate = todayLst[indexPath.item].startAtDateTime {
            s = dateFormatter.stringFromDate(startDate)
            if let endDate = todayLst[indexPath.item].endAtDateTime {
                s +=  " - " + dateFormatter.stringFromDate(endDate)
            }
        }
        else{
            if let endDate = todayLst[indexPath.item].endAtDateTime {
                s += dateFormatter.stringFromDate(endDate)
            }
        }
        cell.lblStartEndDate.text = s
        
        // Show assignees
        s = ""
        if let assignees = todayLst[indexPath.item].assignees {
            if assignees.count > 0 {
                for i in 0..<assignees.count-1 {
                    if assignees[i].first_name != "" {
                        s += assignees[i].first_name
                        if assignees[i].last_name != ""{
                            s += " " + assignees[i].last_name
                        }
                        else{
                            // TODO
                        }
                    }
                    else{
                        if assignees[i].last_name != ""{
                            s += assignees[i].last_name
                        }
                        else{
                            // TODO
                        }
                    }
                    if i < assignees.count {
                        s += " & "
                    }
                    else{
                        // TODO
                    }
                }
            }
        }
        cell.lblAssignees.text = s
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("SegueTaskDetail", sender: self)
    }
    
    // MARK: DAO
    func loadTasks(){
        Service.TaskIndex(Global.User.ID)
        {
            info in
            
            self.lst = []
            self.dueBeforeTodayLst = []
            self.requiringAssignmentLst = []
            self.todayLst = []
            
            if info.Success{
                self.lst=info.List
                let curDate = self.getCurDate()
                
                for i in 0..<self.lst.count {
                    let task=self.lst[i]
                    
                    print(task.endAtDateTime)
                    print(curDate)
                    
                    
                    if let endDate = task.endAtDateTime {
                        if  self.isTaskEnded(endDate, beforeDate: curDate) == 1{
                            self.dueBeforeTodayLst += [task]
                        }
                        else{
                            //
                        }
                        
                        if self.isSameDateEnded(endDate, afterDate: curDate) == 1{
                            
                            self.todayLst += [task]
                            
                        }
                        else{
                            // TODO
                        }
                    }
                    
                    if task.assignedTo == nil{
                        self.requiringAssignmentLst += [task]
                    }
                    else{
                        // TODO
                    }
                }
            }
            
            self.lblNumDueBeforeTodayTasks.text = String(self.dueBeforeTodayLst.count)
            self.lblNumRequiringAssignmentTasks.text = String(self.requiringAssignmentLst.count)
            self.lblNumTodayTasks.text = String(self.todayLst.count)
            self.tableView.reloadData()
        }
    }
    
    func isTaskEnded(date1: NSDate, beforeDate date2:NSDate) -> Int{
        if NSCalendar.currentCalendar().compareDate(date1, toDate: date2,
                                                    toUnitGranularity: .Second) == .OrderedAscending{
            return 1
        }
        else{
            return 0
        }
    }
    
    func isSameDateEnded(date1: NSDate, afterDate date2: NSDate) -> Int {
        let a = NSCalendar.currentCalendar().compareDate(date1, toDate: date2, toUnitGranularity: .Second) == .OrderedDescending
        let b = NSCalendar.currentCalendar().compareDate(date1, toDate: date2, toUnitGranularity: .Second) == .OrderedSame
        let c = (self.dayDiff(date1, withDate: date2) == 0)
        if (a || b) && c {
            return 1
        }
        else{
            return 0
        }
    }
    
    func dayDiff(date1: NSDate, withDate date2: NSDate) -> Int {
        let calendar = NSCalendar.currentCalendar()
        let diff = calendar.components(.NSDayCalendarUnit, fromDate: date1, toDate: date2, options: [])
        return diff.day
    }
    
    func getCurDate() -> NSDate{
        let timezone = 7
        var curDate = NSDate()
        let calendar = NSCalendar.currentCalendar()
        curDate = calendar.dateByAddingUnit(.NSHourCalendarUnit, value: timezone, toDate: curDate, options: .MatchStrictly)!
        return curDate
    }
}
