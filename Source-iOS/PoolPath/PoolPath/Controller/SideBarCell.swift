//
//  SideBarCell.swift
//  PoolPath
//
//  Created by quang on 7/26/16.
//  Copyright © 2016 DnDStudio. All rights reserved.
//

import UIKit

class SideBarCell: UITableViewCell {
    
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblAction: UILabel!
    
}
