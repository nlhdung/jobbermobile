//
//  ClientController.swift
//  PoolPath
//
//  Created by quang on 7/27/16.
//  Copyright © 2016 DnDStudio. All rights reserved.
//
import UIKit

class ClientController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let menuButton = UIBarButtonItem(image: UIImage(named: "reveal-icon"), style: UIBarButtonItemStyle.Plain, target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
        let dashboard = UIBarButtonItem(title: "Clients", style: .Plain, target: nil, action: nil)
        self.navigationItem.setLeftBarButtonItems([menuButton, dashboard], animated: true)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer()) // vuốt để chuyển lại màn hình chính

    }
}