//
//  TimesheetCell.swift
//  PoolPath
//
//  Created by HTThanh on 7/30/16.
//  Copyright © 2016 DnDStudio. All rights reserved.
//

import UIKit

class TimesheetCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblJobDescription: UILabel!
    @IBOutlet weak var lblTime: UILabel!
}
