//
//  BasicTask.swift
//  PoolPath
//
//  Created by quang on 7/25/16.
//  Copyright © 2016 DnDStudio. All rights reserved.
//

import Foundation

class BasicTask {
    var allDay: Bool?
    var assignedTo: [String]?
    var assignees: [Client]?
    var completeBy: String?
    var completedDate: NSDate?
    var createdAt: NSDate?
    var description: String?
    var endAtDateTime: NSDate?
    var eventType: String?
    var id: Int?
    var isCompleted: Bool?
    var propertyId: String?
    var repeated: Bool?
    var startAtDateTime: NSDate?
    var title: String?
    var updatedAt: NSDate?
    var userId: String?
    
    init(){
        self.allDay=false
        self.assignedTo=nil
        self.assignees=nil
        self.completeBy=nil
        self.completedDate=nil
        self.createdAt=nil
        self.description=nil
        self.endAtDateTime=nil
        self.eventType=nil
        self.id=nil
        self.isCompleted=false
        self.propertyId=nil
        self.repeated=nil
        self.startAtDateTime=nil
        self.title=nil
        self.updatedAt=nil
        self.userId=nil
    }
    init(allDay: Bool?, assignedTo: [String]?, assignees: [Client]?, completeBy: String?, completedDate: NSDate?, createdAt: NSDate?, description: String?, endAtDateTime: NSDate?, eventType: String?, id: Int?, isCompleted: Bool?, propertyId: String?, repeated: Bool?, startAtDateTime: NSDate?, startAtTime: NSDate?, title: String?, updatedAt: NSDate?, userId: String?){
        self.allDay=allDay
        self.assignedTo=assignedTo
        self.assignees=assignees
        self.completeBy=completeBy
        self.completedDate=completedDate
        self.createdAt=createdAt
        self.description=description
        self.endAtDateTime=endAtDateTime
        self.eventType=eventType
        self.id=id
        self.isCompleted=isCompleted
        self.propertyId=propertyId
        self.repeated=repeated
        self.startAtDateTime=startAtDateTime
        self.title=title
        self.updatedAt=updatedAt
        self.userId=userId
    }
}