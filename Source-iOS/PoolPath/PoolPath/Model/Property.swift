//
//  var swift
//  PoolPath
//
//  Created by quang on 7/25/16.
//  Copyright © 2016 DnDStudio. All rights reserved.
//

import Foundation

class Property {
    var add_route = false
    var city: String? = ""
    var client_id = -1
    var converted_date : String? = ""
    var country : String? = ""
    var created_at = ""
    var custom_field: String? = ""
    var demo_entry = false
    var estimate_map: String? = ""
    var id = -1
    var latitude: Float? = 0
    var lead = false
    var longitude: Float? = 0
    var market_source : String? = ""
    var pool_activity : String? = ""
    var pool_comment : String? = ""
    
    var pool_gate_code : String? = ""
    var pool_lifeguards: Int? = -1
    var pool_name : String? = ""
    var pool_notes : String? = ""
    
    var pool_source : String? = ""
    var pool_subject : String? = ""
    //var pool_tag = info["pool_tag"] as? String
    var pool_type :String? =  ""
    var pool_volume:String?  = ""
    var pool_volume2 :String? = ""
    var server_plan_id : Int? = -1
    
    var source : String? = ""
    var state = ""
    var street : String? = ""
    var street2 : String?  = ""
    var updated_at = ""
    var user_id = -1
    var zipcode = ""
}