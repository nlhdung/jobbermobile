//
//  Client.swift
//  PoolPath
//
//  Created by quang on 7/25/16.
//  Copyright © 2016 DnDStudio. All rights reserved.
//

import Foundation

class Client {
    var city: String? = ""
    var client_tag: NSArray? = nil
    var company_name = ""
    var country : String? = ""
    var created_at = ""
    var custom_field: String? = ""
    var demo_entry = false
    var email: String? = ""
    var email_initial: String? = ""
    var first_name = ""
    var id = -1
    var initial = ""
    var last_name = ""
    var mobile_number = ""
    var personal_email = ""
    var phone_initial: String? = ""
    var phone_number = ""
    var preference_notification: String? = ""
    var primary_company: String? = ""
    var state : String? = ""
    var street1: String? = ""
    var street2: String? = ""
    var updated_at = ""
    var user_id = -1
    var zip_code: String? = ""
}