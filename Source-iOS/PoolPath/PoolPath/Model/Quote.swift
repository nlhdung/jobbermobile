//
//  Quote.swift
//  PoolPath
//
//  Created by quang on 7/25/16.
//  Copyright © 2016 DnDStudio. All rights reserved.
//

import Foundation

class Quote {
    var archive = false
    var client_message: String? = ""
    var created_at: String? = ""
    var custom_field: String? = ""
    var demo_entry = false
    var discount : Int? = -1
    var discount_type: String? = ""
    var id: Int?
    var is_mailed = false
    var quote_order: Int?
    var raty_score: String? = ""
    
    class Works {
        var created_at : String? = ""
        var description = NSArray()
        var id = -1
        var name = NSArray()
        var quantity = NSArray()
        var quote_id = -1
        var total = NSArray()
        var unit_cost = NSArray()
        var updated_at: String? = ""
    }
    
    var works = Works()
    
    var require_deposit: String? = ""
    var require_deposit_type: String? = ""
    var sent = false
    var tax : Int? = -1
    var updated_at : String? = ""
    var user_id = -1
    
    

}