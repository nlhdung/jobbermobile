//
//  Service.swift
//  PickEasy
//
//  Created by Quang Tran on 4/15/16.
//  Copyright © 2016 DelayQueue. All rights reserved.
//

import UIKit
import Alamofire

class Service
{
    //MARK: USER API
    
    // Success - Optional({
    //      "first_name" = vivek;
    //      "last_name" = soni;
    //      status = Success;
    //      "user_id" = 61;
    // })
    // Fail - Optional({
    //    status = "Invalid email or password.";
    // })
    static func Login(email: String, password: String,
                      handler: ((Success: Bool, Status: String))->Void) {
        
        let manager = Alamofire.Manager.sharedInstance
        manager.request(.POST, URL.Login,
            parameters: ["email": email,"password": password]).responseJSON {
                response in
                
                print(response)
                
                var success = false
                var status = ""
                
                if let json = response.result.value {
                    status = json["status"] as! String
                    
                    success = status == "Success"
                    
                    if success {
                       Global.User.ID = json["user_id"] as! Int
                       Global.User.FirstName = json["first_name"] as! String
                       Global.User.LastName = json["last_name"] as! String
                    }
                }
                handler((success, status))
        }
    }

    // Nếu truyền vào địa chỉ email KHÔNG hợp lệ
    // SUCCESS: {
    //      status = "Please login first";
    // }
    static func Logout() {
        Alamofire.request(.DELETE, URL.SignOut,
            parameters: nil).responseJSON {
                response in
                
            print(response)
        }
    }
    
    ///////////////////////////////////////////////////////
    // MARK: CLIENT API
  
    // {"id":3,"initial":"Mr","first_name":"Quang","last_name":"Teo","company_name":"ABC","primary_company":null,"street1":null,"street2":null,"city":null,
    // "state":null,"zip_code":null,"country":null,"phone_initial":null,"phone_number":null,"email_initial":null,"email":null,"created_at":"2016-08-01T08:42:39.903Z",
    // "updated_at":"2016-08-01T08:42:39.903Z","user_id":61,"client_tag":[],"custom_field":null,"mobile_number":"78612115","personal_email":"tdquang7@gmail.com",
    // "preference_notification":null,"demo_entry":true}
    static func ClientCreate(initial: String, firstname: String, lastname: String, company: String, email: String, tel: String,
                             handler: ((Success: Bool, Status: String))->Void) {
       Global.Web.request(.POST, URL.ClientCreate + String(Global.User.ID),
            parameters: ["initial": initial, "first_name": firstname, "last_name": lastname, "company_name": company, "personal_email": email, "mobile_number": tel], encoding: .JSON).responseData {
                response in
                
                var success = false
                var status = ""
                
                success = (response.data?.length == 0)
                if !success {
                    status = String(data: response.data!, encoding: NSUTF8StringEncoding)!
                }
                
                handler((success, status))
        }
    }
    
    static func ClientShow(clientid: String,
                           handler: (client: Client)->Void){
        let url = URL.ClientShow.stringByReplacingOccurrencesOfString(":id", withString: String(clientid)) + String(Global.User.ID)
        
        Global.Web.request(.GET, url).responseJSON{
            response in
            print ("Test ClientShow")
            print (response)
            print (clientid)
            
            if let info = response.result.value {
                let client = Client()
                client.city = info["city"] as? String
                client.client_tag = info["client_tag"] as? NSArray
                client.company_name = info["company_name"] as! String
                client.country = info["country"] as? String
                client.created_at = info["created_at"] as! String
                client.custom_field = info["custom_field"] as? String
                client.demo_entry = info["demo_entry"] as! Bool
                client.email = info["email"] as? String
                client.email_initial = info["email_initial"] as? String
                client.first_name = info["first_name"] as! String
                client.id = info["id"] as! Int
                client.initial = info["initial"] as! String
                client.last_name = info["last_name"] as! String
                client.mobile_number = info["mobile_number"] as! String
                client.personal_email = info["personal_email"] as! String
                client.phone_initial = info["phone_initial"] as? String
                client.phone_number = String(info["phone_number"])
                client.preference_notification = info["preference_notification"] as? String
                client.primary_company = info["primary_company"] as? String
                client.state = info["state"] as? String
                client.street1 = info["street1"] as? String
                client.street2 = info["street2"] as? String
                client.updated_at = info["updated_at"] as! String
                client.user_id = info["user_id"] as! Int
                client.zip_code = info["zip_code"] as? String
                
                handler(client: client)
            }
        }
    }
    
    // Xem file Documents/ClientIndexResult.js để có kết quả json mẫu
    static func ClientIndex(handler: ([Client])->Void){
        Global.Web.request(.GET, URL.ClientIndex + String(Global.User.ID)).responseJSON{
            response in

            if let clients = response.result.value {
                var list = [Client]()
                
                for i in 0..<clients.count {
                    let client = Client()
                    let info = clients[i]
                    client.city = info["city"] as? String
                    client.client_tag = info["client_tag"] as? NSArray
                    client.company_name = info["company_name"] as! String
                    client.country = info["country"] as? String
                    client.created_at = info["created_at"] as! String
                    client.custom_field = info["custom_field"] as? String
                    client.demo_entry = info["demo_entry"] as! Bool
                    client.email = info["email"] as? String
                    client.email_initial = info["email_initial"] as? String
                    client.first_name = info["first_name"] as! String
                    client.id = info["id"] as! Int
                    client.initial = info["initial"] as! String
                    client.last_name = info["last_name"] as! String
                    client.mobile_number = info["mobile_number"] as! String
                    client.personal_email = info["personal_email"] as! String
                    client.phone_initial = info["phone_initial"] as? String
                    client.phone_number = String(info["phone_number"])
                    client.preference_notification = info["preference_notification"] as? String
                    client.primary_company = info["primary_company"] as? String
                    client.state = info["state"] as? String
                    client.street1 = info["street1"] as? String
                    client.street2 = info["street2"] as? String
                    client.updated_at = info["updated_at"] as! String
                    client.user_id = info["user_id"] as! Int
                    client.zip_code = info["zip_code"] as? String
                    
                    list += [client]
                }
                
                handler(list)
            }
        }
    }
    
    // TODO: Đang hỏi client
    static func ClientEdit(){
        Global.Web.request(.GET, URL.ClientEdit + String(Global.User.ID)).responseJSON{
            response in

            print(response)
        }
    }
    
    ///////////////////////////////////////
    // MARK: TASK API
    
    static func TaskCreate(title: String, allDay: Int, startAtDate: String, startAtTime: String, endAtDate: String, endAtTime: String, description: String, userId: Int,
                           handler: ((Success: Bool, Status: String))->Void) {
        Global.Web.request(.POST, URL.TaskCreate + String(userId),
            parameters: ["title": title, "all_day": allDay, "start_at_date": startAtDate, "start_at_time": startAtTime, "end_at_date": endAtDate, "end_at_time": endAtTime, "description": description,"user_id": userId], encoding: .JSON).responseJSON {
                response in
                
                print("Test TaskCreate")
                print(response)
                
                var msg: String = ""
                
                var success = false
                if let json = response.result.value as? NSDictionary {
                    if json.allKeys.count == 1 {
                        success = false
                        msg = json["status"]!.stringValue
                    }
                    else{
                        success = true
                    }
                }
                handler((success, msg))
        }
    }
    
    
    // userID: 61 -> "Basic task not found"
    static func TaskShow(userId: Int,
                         handler: ((Success: Bool, List: [BasicTask]))->Void) {
        Global.Web.request(.GET, URL.TaskShow.stringByReplacingOccurrencesOfString(":id", withString: String(userId)),
            parameters: ["user_id": userId]).responseJSON {
                response in
                var success = false
                var list = [BasicTask]()
                
                if let tasks = response.result.value {
                    if !(tasks is NSArray) {
                        success = false
                    }
                    else{
                        success = true
                        let datetimeFormatter = NSDateFormatter()
                        datetimeFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        
                        for i in 0..<tasks.count {
                            let task = BasicTask()
                            let info = tasks[i]
                            
                            task.allDay = info["all_day"] as? Bool
                            if let assignedTo = info["assigned_to"]{
                                if assignedTo is NSArray {
                                    task.assignedTo = []
                                    task.assignees = []
                                    print(assignedTo?.count)
                                    for j in 0..<assignedTo!.count {
                                        let x =  assignedTo![j] as! String
                                        task.assignedTo?.append(x)
//                                        Service.ClientShow(x)
//                                        {
//                                            info in
//                                            task.assignees?.append(info)
//                                        }
                                    }
                                }
                            }
                            task.completeBy = info["complete_by"] as? String
                            if let completedDate = info["completed_date"] as? String {
                                //if !(completedDate is NSNull) {
                                task.completedDate = datetimeFormatter.dateFromString(completedDate)
                                //}
                            }
                            if let createdAt = info["created_at"] as? String {
                                // if !(createdAt is NSNull) {
                                task.createdAt = datetimeFormatter.dateFromString(createdAt)
                                // }
                            }
                            task.description = info["description"] as? String
                            
                            if let endAtDateStr = info["end_at_date"] as? String{
                                if var endAtTimeStr = info["end_at_time"] as? String {
                                    endAtTimeStr = endAtTimeStr.stringByReplacingOccurrencesOfString("2000-01-01", withString: endAtDateStr)
                                    task.endAtDateTime = datetimeFormatter.dateFromString(endAtTimeStr )
                                }
                                else{
                                    task.endAtDateTime = dateFormatter.dateFromString(endAtDateStr )
                                }
                                //                                let calendar = NSCalendar.currentCalendar()
                                //                                var x = calendar.component(.Day, fromDate: task.endAtDateTime!)
                                //                                print (task.endAtDateTime)
                                //                                x = x - 1
                                //                                print (x)
                                //                                if x != 12 {
                                //                                    continue
                                //                                }
                            }
                            else{
                                if let endAtTimeStr = info["end_at_time"] as? String{
                                    task.endAtDateTime = datetimeFormatter.dateFromString(endAtTimeStr)
                                    //                                    let calendar = NSCalendar.currentCalendar()
                                    //                                    var x = calendar.component(.Day, fromDate: task.endAtDateTime!)
                                    //                                    print (task.endAtDateTime)
                                    //                                    x = x - 1
                                    //                                    print (x)
                                    //                                    if x != 12 {
                                    //                                        continue
                                    //                                    }
                                    
                                }
                                else{
                                    task.endAtDateTime = nil
                                    //                                    continue
                                }
                                
                            }
                            if let completedDate = info["completed_date"] as? String {
                                // if !(completedDate is NSNull) {
                                task.completedDate = datetimeFormatter.dateFromString(completedDate)
                                // }
                            }
                            task.eventType = info["event_type"] as? String
                            task.id = info["id"] as? Int
                            task.isCompleted = info["is_completed"] as? Bool
                            task.propertyId = info["property_id"] as? String
                            task.repeated = info["repeat"] as? Bool
                            if let startAtDateStr = info["start_at_date"] as? String {
                                if var startAtTimeStr = info["start_at_time"] as? String {
                                    startAtTimeStr = startAtTimeStr.stringByReplacingOccurrencesOfString("2000-01-01", withString: startAtDateStr )
                                    task.startAtDateTime = datetimeFormatter.dateFromString(startAtTimeStr )
                                }
                                else{
                                    task.startAtDateTime = dateFormatter.dateFromString(startAtDateStr )
                                }
                                
                            }
                            else{
                                if let startAtTimeStr = info["start_at_time"] as? String {
                                    task.startAtDateTime = datetimeFormatter.dateFromString(startAtTimeStr )
                                }
                                else{
                                    task.startAtDateTime = nil
                                }
                                
                            }
                            task.title = info["title"] as? String
                            task.updatedAt = info["updated_at"] as? NSDate
                            task.userId = info["user_id"] as? String
                            
                            list += [task]
                        }
                    }
                    
                }
                handler((success, list))

        }
    }
    
    static func TaskIndex(userId: Int,
                           handler: ((Success: Bool, List: [BasicTask]))->Void) {
        Global.Web.request(.GET, URL.TaskIndex + String(userId),
            parameters: ["user_id": userId]).responseJSON {
                response in
                print("Test TaskIndex")
                print(response)
                
                var success = false
                var list = [BasicTask]()
                
                if let tasks = response.result.value {
                    if !(tasks is NSArray) {
                        success = false
                    }
                    else{
                        success = true
                        let datetimeFormatter = NSDateFormatter()
                        datetimeFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        
                        for i in 0..<tasks.count {
                            let task = BasicTask()
                            let info = tasks[i]
                            
                            task.allDay = info["all_day"] as? Bool
                            if let assignedTo = info["assigned_to"]{
                                if assignedTo is NSArray {
                                    task.assignedTo = []
                                    task.assignees = []
                                    print(assignedTo?.count)
                                    for j in 0..<assignedTo!.count {
                                        let x =  assignedTo![j] as! String
                                        task.assignedTo?.append(x)
//                                        Service.ClientShow(x)
//                                        {
//                                            info in
//                                            task.assignees?.append(info)
//                                        }
                                    }
                                }
                            }
                            task.completeBy = info["complete_by"] as? String
                            if let completedDate = info["completed_date"] as? String {
                                //if !(completedDate is NSNull) {
                                    task.completedDate = datetimeFormatter.dateFromString(completedDate)
                                //}
                            }
                            if let createdAt = info["created_at"] as? String {
                                // if !(createdAt is NSNull) {
                                    task.createdAt = datetimeFormatter.dateFromString(createdAt)
                                // }
                            }
                            task.description = info["description"] as? String
                            
                            if let endAtDateStr = info["end_at_date"] as? String{
                                if var endAtTimeStr = info["end_at_time"] as? String {
                                    endAtTimeStr = endAtTimeStr.stringByReplacingOccurrencesOfString("2000-01-01", withString: endAtDateStr)
                                    task.endAtDateTime = datetimeFormatter.dateFromString(endAtTimeStr )
                                }
                                else{
                                    task.endAtDateTime = dateFormatter.dateFromString(endAtDateStr )
                                }
//                                let calendar = NSCalendar.currentCalendar()
//                                var x = calendar.component(.Day, fromDate: task.endAtDateTime!)
//                                print (task.endAtDateTime)
//                                x = x - 1
//                                print (x)
//                                if x != 12 {
//                                    continue
//                                }
                            }
                            else{
                                if let endAtTimeStr = info["end_at_time"] as? String{
                                    task.endAtDateTime = datetimeFormatter.dateFromString(endAtTimeStr)
//                                    let calendar = NSCalendar.currentCalendar()
//                                    var x = calendar.component(.Day, fromDate: task.endAtDateTime!)
//                                    print (task.endAtDateTime)
//                                    x = x - 1
//                                    print (x)
//                                    if x != 12 {
//                                        continue
//                                    }

                                }
                                else{
                                    task.endAtDateTime = nil
//                                    continue
                                }
                                
                            }
                            if let completedDate = info["completed_date"] as? String {
                                // if !(completedDate is NSNull) {
                                    task.completedDate = datetimeFormatter.dateFromString(completedDate)
                                // }
                            }
                            task.eventType = info["event_type"] as? String
                            task.id = info["id"] as? Int
                            task.isCompleted = info["is_completed"] as? Bool
                            task.propertyId = info["property_id"] as? String
                            task.repeated = info["repeat"] as? Bool
                            if let startAtDateStr = info["start_at_date"] as? String {
                                if var startAtTimeStr = info["start_at_time"] as? String {
                                    startAtTimeStr = startAtTimeStr.stringByReplacingOccurrencesOfString("2000-01-01", withString: startAtDateStr )
                                    task.startAtDateTime = datetimeFormatter.dateFromString(startAtTimeStr )
                                }
                                else{
                                    task.startAtDateTime = dateFormatter.dateFromString(startAtDateStr )
                                }
                                
                            }
                            else{
                                if let startAtTimeStr = info["start_at_time"] as? String {
                                    task.startAtDateTime = datetimeFormatter.dateFromString(startAtTimeStr )
                                }
                                else{
                                    task.startAtDateTime = nil
                                }
                                
                            }
                            task.title = info["title"] as? String
                            task.updatedAt = info["updated_at"] as? NSDate
                            task.userId = info["user_id"] as? String
                            
                            list += [task]
                        }
                    }
                    
                }
                handler((success, list))
        }
    }
    
    //////////////////////////////////
    // MARK: Property API
    
    static func PropertyShow(id: Int, handler: (property: Property)->Void){
        let url = URL.PropertyShow.stringByReplacingOccurrencesOfString(":id", withString: String(id)) + String(Global.User.ID)
        Global.Web.request(.GET, url).responseJSON{
            response in
            
            if let info = response.result.value {
                let property = Property()
                property.add_route = info["add_route"] as! Bool
                property.city = info["city"] as? String
                property.client_id = info["client_id"] as! Int
                property.converted_date = info["converted_date"] as? String
                property.country = info["country"] as? String
                property.created_at = info["created_at"] as! String
                property.custom_field = info["custom_field"] as? String
                property.demo_entry = info["demo_entry"] as! Bool
                property.estimate_map = info["estimate_map"] as? String
                property.id = info["id"]  as! Int
                property.latitude = info["latitude"] as? Float
                property.lead = info["lead"] as! Bool
                property.longitude = info["longitude"] as? Float
                property.market_source = info["market_source"] as? String
                property.pool_activity = info["pool_activity"] as? String
                property.pool_comment = info["pool_comment"] as? String
                
                property.pool_gate_code = info["pool_gate_code"] as? String
                property.pool_lifeguards = info["pool_lifeguards"] as? Int
                property.pool_name = info["pool_name"] as? String
                property.pool_notes = info["pool_notes"] as? String
                
                property.pool_source = info["pool_source"] as? String
                property.pool_subject = info["pool_subject"] as? String
                //property.pool_tag = info["pool_tag"] as? String //TODO: Mảng rỗng
                property.pool_type = info["pool_type"] as? String
                property.pool_volume = info["pool_volume"] as? String
                property.pool_volume2 = info["pool_volume2"]  as? String
                property.server_plan_id = info["server_plan_id"] as? Int
                
                property.source = info["source"] as? String
                property.state = info["state"] as! String
                property.street = info["street"] as? String
                property.street2 = info["street2"] as? String
                property.updated_at = info["updated_at"] as! String
                property.user_id = info["user_id"] as! Int
                property.zipcode = info["zipcode"] as! String
                
                handler(property: property)
            }
        }
    }
    
    
    static func PropertyIndex(handler: ([Property])->Void){
        let url = URL.PropertyIndex + String(Global.User.ID)
        Global.Web.request(.GET, url, parameters: nil).responseJSON {
            response in
            
            if let properties = response.result.value {
                var list = [Property]()
                
                for i in 0..<properties.count {
                    let info = properties[i]
                    
                    let property = Property()
                    property.add_route = info["add_route"] as! Bool
                    property.city = info["city"] as? String
                    property.client_id = info["client_id"] as! Int
                    property.converted_date = info["converted_date"] as? String
                    property.country = info["country"] as? String
                    property.created_at = info["created_at"] as! String
                    property.custom_field = info["custom_field"] as? String
                    property.demo_entry = info["demo_entry"] as! Bool
                    property.estimate_map = info["estimate_map"] as? String
                    property.id = info["id"]  as! Int
                    property.latitude = info["latitude"] as? Float
                    property.lead = info["lead"] as! Bool
                    property.longitude = info["longitude"] as? Float
                    property.market_source = info["market_source"] as? String
                    property.pool_activity = info["pool_activity"] as? String
                    property.pool_comment = info["pool_comment"] as? String
                    
                    property.pool_gate_code = info["pool_gate_code"] as? String
                    property.pool_lifeguards = info["pool_lifeguards"] as? Int
                    property.pool_name = info["pool_name"] as? String
                    property.pool_notes = info["pool_notes"] as? String
                    
                    property.pool_source = info["pool_source"] as? String
                    property.pool_subject = info["pool_subject"] as? String
                    //property.pool_tag = info["pool_tag"] as? String //TODO: Mảng rỗng
                    property.pool_type = info["pool_type"] as? String
                    property.pool_volume = info["pool_volume"] as? String
                    property.pool_volume2 = info["pool_volume2"]  as? String
                    property.server_plan_id = info["server_plan_id"] as? Int
                    
                    property.source = info["source"] as? String
                    property.state = info["state"] as! String
                    property.street = info["street"] as? String
                    property.street2 = info["street2"] as? String
                    property.updated_at = info["updated_at"] as! String
                    property.user_id = info["user_id"] as! Int
                    property.zipcode = info["zipcode"] as! String
                    
                    list += [property]
                }
                
                handler(list)
            }
        }
    }
    
    static func PropertyCreate(street: String, city: String, state: String, zipcode: String, country: String, clientid: Int,
                               handler: ((Success: Bool, Status: String))->Void){
        let url = URL.PropertyCreate + String(Global.User.ID)
        Global.Web.request(.POST, url,
            parameters: ["street": street,"city": city,"state": state,"zipcode": zipcode,"country": country,"client_id": String(clientid)], encoding: .JSON).responseJSON {
            response in

            let success = true
            var status = "OK"
                
            if let info = response.result.value {
                if info["status"] != nil {
                    status = info["status"] as! String
                }
                
                handler((success, status))
            }
        }
    }
    
    ///////////////////////////////
    // MARK: Quote API
    
    static func QuoteIndex(handler: ([Quote])->Void){
        let url = URL.QuoteIndex + String(Global.User.ID)
        Global.Web.request(.GET, url).responseJSON {
            response in
            
            
            if let quotes = response.result.value {
                var list = [Quote]()
                
                for i in 0..<quotes.count {
                    let info = quotes[i]
                    let quote = Quote()
                    
                    quote.client_message = info["client_message"] as? String
                    quote.discount = info["discount"] as? Int
                    quote.discount_type = info["discount_type"] as? String
                    quote.id = info["id"] as? Int
                    quote.quote_order = info["quote_order"] as? Int

                    let works =  info["quote_works"] as! NSArray
                    
                    if (works.count > 0) {
                        let work = works[0] as! NSDictionary
                        
                        quote.works.description = work["description"] as! NSArray
                        quote.works.id = work["id"] as! Int
                        quote.works.name = work["name"] as! NSArray
                        quote.works.quantity = work["quantity"] as! NSArray
                        quote.works.quote_id = work["quote_id"] as! Int
                        quote.works.total = work["total"] as! NSArray
                        quote.works.unit_cost = work["unit_cost"] as! NSArray
                    }
                    
                    quote.require_deposit = info["require_deposit"] as? String
                    quote.require_deposit_type = info["require_deposit_type"] as? String
                    quote.tax = info["tax"] as? Int
                    
                    list += [quote]
                }
                
                handler(list)
            }
        }
    }
    
    static func QuoteShow(id: Int, handler: (Quote)->Void){
        let url = URL.QuoteShow.stringByReplacingOccurrencesOfString(":id", withString: String(id)) + String(Global.User.ID)
        Global.Web.request(.GET, url).responseJSON {
            response in

            if let json = response.result.value {
                let info = json["quote"] as! NSDictionary
                let quote = Quote()
                quote.archive = info["archive"] as! Bool
                quote.client_message = info["client_message"] as? String
                quote.created_at = info["created_at"] as? String
                quote.custom_field = info["custom_field"] as? String
                quote.demo_entry = info["demo_entry"] as! Bool
                quote.discount = info["discount"] as? Int
                quote.discount_type = info["discount_type"] as? String
                quote.id = info["id"] as? Int
                quote.is_mailed = info["is_mailed"] as! Bool
                quote.quote_order = info["quote_order"] as? Int
                quote.raty_score = info["raty_score"] as? String
                quote.require_deposit = info["require_deposit"] as? String
                quote.require_deposit_type = info["require_deposit_type"] as? String
                quote.sent = info["sent"] as! Bool
                quote.tax = info["tax"] as? Int
                quote.updated_at = info["updated_at"] as? String
                quote.user_id = info["user_id"] as! Int
                
                let works =  json["quote_work"] as! NSArray
                let work = works[0] as! NSDictionary
                quote.works.created_at = work["created_at"] as? String
                quote.works.description = work["description"] as! NSArray
                quote.works.id = work["id"] as! Int
                quote.works.name = work["name"] as! NSArray
                quote.works.quantity = work["quantity"] as! NSArray
                quote.works.quote_id = work["quote_id"] as! Int
                quote.works.total = work["total"] as! NSArray
                quote.works.unit_cost = work["unit_cost"] as! NSArray
                quote.works.updated_at = work["updated_at"] as? String

                handler(quote)
            }
        }
    }
    
    static func QuoteCreate(property_id: String, quote_order: Int, tax: Int, discount: Int, work_order_name: String, work_order_description: String, client_message: String ){
        let url = URL.QuoteCreate + String(Global.User.ID)
        
        Global.Web.request(.POST, url, parameters: ["property_id":property_id, "quote_order": String(quote_order), "tax": String(tax),"discount": String(discount),"work_order_name": work_order_name,"work_order_description": work_order_description, "client_message": client_message], encoding: .JSON).responseData{
            response in
            
            // Hàm này thành công trả lại SUCCESS: <> nên thôi khỏi handler chi cho mệt :v
        }
    }

}