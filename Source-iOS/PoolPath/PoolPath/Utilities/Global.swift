//
//  Global.swift
//  PoolPath
//
//  Created by quang on 7/25/16.
//  Copyright © 2016 DnDStudio. All rights reserved.
//
import Alamofire

class Global {
    static var DEBUG = true
    
    class User {
        static var ID = -1
        static var FirstName = ""
        static var LastName = ""
        
        static func Logout() {
            ID = -1
            FirstName = ""
            LastName = ""
        }
    }
    
    class TestData {
        static var Email = "vivekyuvasoft115@gmail.com"
        static var Password = "12345678"

    }
    
    static var Web: Manager{
        get { return Alamofire.Manager.sharedInstance}
    }
}