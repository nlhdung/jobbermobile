//
//  URL.swift
//  Timevy
//
//  Created by Dung L.H. Nguyen on 7/4/16.
//  Copyright © 2016 DnDStudio. All rights reserved.
//

import Foundation

import UIKit
import Alamofire

class URL
{
    static let ROOT = "http://45.55.157.4/api/v1/"
    
    //MARK: User
    /* 1*/ static let Login = ROOT + "sign_in"
    /* 2*/ static let SignOut = ROOT + "sign_out"
    /* 3*/ static let ForgetPassword = ROOT + "users/password"
    
    // MARK: Basic task
    /* 4*/ static let TaskCreate = ROOT + "basic_tasks?user_id="
    /* 5*/ static let TaskShow = ROOT + "basic_tasks/:id?user_id="
    /* 6*/ static let TaskEdit = ROOT + "basic_tasks/:id/edit?user_id="
    /* 7*/ static let TaskIndex = ROOT + "basic_tasks?user_id="
    /* 8*/ static let TaskDestroy = ROOT + "basic_tasks/:id?user_id="
    
    // MARK: Client
    /* 9*/ static let ClientCreate = ROOT + "clients?user_id="
    /*10*/ static let ClientShow = ROOT + "clients/:id?user_id="
    /*11*/ static let ClientDelete = ROOT + "clients/:id?user_id="
    /*12*/ static let ClientEdit = ROOT + "clients/:id/edit?user_id="
    /*13*/ static let ClientIndex = ROOT + "clients?user_id="
    
    // MARK: Property
    /*14*/ static let PropertyCreate = ROOT + "properties?user_id="
    /*15*/ static let PropertyShow = ROOT + "properties/:id?user_id="
    /*16*/ static let PropertyEdit = ROOT + "properties/:id/edit?user_id="
    /*17*/ static let PropertyDelete = ROOT + "properties/:id?user_id="
    /*18*/ static let PropertyIndex = ROOT + "properties?user_id="
    
    // MARK: Quote
    /*19*/ static let QuoteCreate = ROOT + "quotes?user_id="
    /*20*/ static let QuoteShow = ROOT + "quotes/:id?user_id="
    /*21*/ static let QuoteEdit = ROOT + "quotes/:id/edit?user_id="
    /*22*/ static let QuoteIndex = ROOT + "quotes?user_id="
    /*23*/ static let QuoteDestroy = ROOT + "quotes/:id?user_id="
    
    // MARK: Job
    /*24*/ static let JobCreate = ROOT + "jobs?user_id="
    /*25*/ static let JobShow = ROOT + "jobs/:id?user_id="
    /*26*/ static let JobEdit = ROOT + "jobs/:id/edit?user_id="
    /*27*/ static let JobIndex = ROOT + "jobs?user_id="
    /*28*/ static let JobDestroy = ROOT + "jobs/:id"
    
    // MARK: Invoice
    /*29*/ static let InvoiceCreate = ROOT + "invoices?user_id="
    /*30*/ static let InvoiceShow = ROOT + "invoices/:id?user_id="
    /*31*/ static let InvoiceEdit = ROOT + "invoices/:id/edit?user_id="
    /*32*/ static let InvoiceIndex = ROOT + "invoices?user_id="
    /*33*/ static let InvoiceDestroy = ROOT + "invoices/:id"
    
    // MARK: Exxpense
    /*34*/ static let ExpenseCreate = ROOT + "expenses?user_id="
    /*35*/ static let ExpenseCreateDistanceOrMileage = ROOT + "expenses?user_id="
    /*36*/ static let ExpenseEdit = ROOT + "expenses/:id/edit?user_id="
    /*37*/ static let ExpenseUpdate = ROOT + "expenses/:id?user_id="
    /*38*/ static let ExpenseShow = ROOT + "expenses/:id?user_id="
    /*39*/ static let ExpenseDestroy = ROOT + "expenses/:id?user_id="
    /*40*/ static let ExpenseIndex = ROOT + "expenses?user_id="
    
    // MARK: TimeSheet
    /*41*/ static let TimesheetIndex = ROOT + "timesheets?user_id="
    /*42*/ static let TimesheetCreate = ROOT +  "timesheets?user_id="
    /*43*/ static let TimesheetEdit = ROOT + "timesheets:id/edit?user_id="
    /*44*/ static let TimesheetUpdate = ROOT + "timesheets/:id/update_timesheet?user_id="
    /*45*/ static let TimesheetDestroy = ROOT + "timesheets/:id?user_id="
    /*46*/ static let TimesheetShow = ROOT + "timesheets/:id?user_id="
    
    // MARK: Inventory
    /*47*/ static let InventoryCreate = ROOT + "inventories?user_id="
    /*48*/ static let InventoryDelete = ROOT + "inventories/:id?user_id="
    /*49*/ static let InventoryIndex = ROOT + "inventories?user_id="
    /*50*/ static let InventoryShow = ROOT + "inventories/1?user_id=" // so 1???
    
    // MARK: Today Schedule
    /*51*/static let TodaySchedule = ROOT + "jobs/today_schedule?user_id="
    
    // MARK: Chemical
    /*52*/static let ChemicalInbox = ROOT + "chemical_add?user_id="
    /*53*/static let ChemicalAdd = ROOT + "chemical_used?user_id="
    /*54*/static let ChemicalAddView = ROOT + "chemical_disy?user_id=UID&property_id=PID"
    
    // MARK: Test web services
    /*55*/ static let TestCreate = ROOT + "chemical_test?user_id=1&property_id=1"
    /*56*/ static let ViewChemicalTestResult = ROOT + "view_chemical_result?user_id=1&property_id=1"
    /*57*/ static let ViewChemicalTestRange = ROOT + "chemical_test_range?user_id=1"
    /*58*/ static let ServiceNewDisplay = ROOT + "service_item?user_id=1&property_id=1"
    /*59*/ static let ServiceItemCreate = ROOT + "service_item_create?user_id=1&property_id=1"
}
