//
//  UI.swift
//  PoolPath
//
//  Created by quang on 7/26/16.
//  Copyright © 2016 DnDStudio. All rights reserved.
//

import UIKit

class UI {
    static var Root = UINavigationController()
    
    static func Alert(title: String, message: String, ui: UIViewController){
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let okAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertView.addAction(okAction)
        
        ui.presentViewController(alertView, animated: true, completion: nil)
    }
    
    static func Segue(ui: UIViewController, id: String){
        ui.performSegueWithIdentifier(id, sender: nil)
    }
    
    static func Get(idStoryBoard : String, idVC : String) -> UIViewController {
        let mainStoryboard = UIStoryboard(name: idStoryBoard,bundle: nil)
        return mainStoryboard.instantiateViewControllerWithIdentifier(idVC)
    }
}
