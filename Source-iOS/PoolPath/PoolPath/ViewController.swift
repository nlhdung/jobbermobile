//
//  ViewController.swift
//  PoolPath
//
//  Created by Dung L.H. Nguyen on 7/25/16.
//  Copyright © 2016 DnDStudio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view, typically from a nib.
       
        if Global.DEBUG {
            txtEmail.text = Global.TestData.Email
            txtPassword.text = Global.TestData.Password
        }
    }
    
    @IBAction func btnLogin_Tapped(sender: UIButton) {
        Service.Login(txtEmail.text!, password: txtPassword.text!)
        {
            info in
            if !info.Success {
                UI.Alert("Error", message: info.Status, ui: self)
            }
            else {
                self.performSegueWithIdentifier("SegueDashboard", sender: self)
            }
            
        }
    }    
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}

